import io
import socket
import sys
import json
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1',56652))
maxlisten = 5
s.listen(maxlisten)
msg='{"code":"#include <iostream> \n using namespace std;int main(){int a,b;cin>>a>>b;cout<<a+b;return 0;}","lan":"2","n":"3","id":"0001","in1":"1 1","in2":"1 2","out1":"2","out2":"3"}'
print(msg)
while True:
    # 建立客户端连接
    clientsocket, addr = s.accept()
    print("连接地址: %s" % str(addr))

    file = io.open('json.txt', "r")
    msg = file.read()

    print((json.dumps(msg)).encode('utf-8'))

    clientsocket.send( repr(json.dumps(msg)).encode(encoding="utf-8"))

    #socket传递的是字节集，它无法实习文本的传递，因此，在传递前，要先把文本转换为字节集：string.encode('utf-8')
    clientsocket.close()