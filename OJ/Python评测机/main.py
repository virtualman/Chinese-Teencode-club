#!/usr/bin/python3
#! -*- coding: utf8 -*-
import re
import os
import socket
import sys
import json
import json.encoder
import io
import pexpect
import datetime
import Evaluation

print("评测机启动成功，当前操作系统为："+sys.platform)
#GetDataFromServer()：获取服务器传递来的数据;

def GetDataFromServer():
    data = new_socket.recv(500000)
    print((data).decode())
    #if str(data)!='':
        #new_socket.send('OK'.encode(encoding='utf-8'))
    #input()
    return ((data).decode())

def Return_Server_res(errorid,all_res={}):
    if(errorid=='CE'):
        new_socket.send("CE".encode(encoding='utf-8'))
        return
    else:
        new_socket.send('CF'.encode(encoding='utf-8'))#否则发送CF：编译成功，正在运行，然后服务器那边准备下接收下面N条数据
    for i in range(1,len(all_res)+1):
        new_socket.send(all_res[i].encode(encoding='utf-8'))


def SendMesToServer(string,csocket):
    try:
        csocket.send(string.encode(encodeing='utf-8'))
        print("评测结果返回成功！")
    except:
        print("评测结果返回失败，具体原因不详！")

ip=open("./data/host_ip.txt","r").read()
point = open("./data/host_point.txt","r").read()
address = (ip,int(point))
new_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
new_socket.connect(address)
for q in range(1,2):
    #变量初始化 Start：
    all_res = {}
    all_res_memory = {}
    all_res_time = {}
    ce = 0
    #变量初始化 End;
    data_json=json.loads(GetDataFromServer())

    data_code = data_json['code']
    data_id = data_json['problemid']
    data_n = data_json['n']
    data_lan = data_json['lan']
    data_time = data_json['time']
    data_memory = data_json['memory']
    data_in=data_json['in']
    data_out=data_json['out']
    data_logid=data_json['logid']

    #数据都从服务器获取成功了
    #下面开始启动评测函数
    print('开始评测：')
    Evaluation(data_code,data_lan,data_n,data_in,data_out,data_memory,all_res,all_res_time,all_res_memory)
    SendMesToServer("OK",new_socket)
    #SendEvaluationAnsToServer(data_logid, ce,all_res,errorcode,all_res_time,all_res_memory)
    new_socket.close()

